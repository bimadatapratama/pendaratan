## README! ##

SCRIPTS Migrasi Data Produksi dan Aktivitas PIPP ke App Pendaratan Ikan.


**Table target semua ditruncate dulu. 
AutoIncrement juga harus direset.**

**Karena id_aktivitas_kapal yang sudah referensi di tabel lain.**

**Table theader_pelabuhan tidak dihandle di script ini**

### Daftar Tabel ###
```
Tabel yang dimigrasi
---------------------
trs_aktivitas_kapal
trs_alat_bantu_keluar
trs_maksud_kunjungan
trs_perbekalan_kapal_keluar
trs_produksi

```


### STEPS ###
1. Semua file *.sh harus executable ( chmod +x )
2. Set mysql password di file run_sql.sh
2. Jalankan run_sql.sh 
```
./run_sql.sh
```

### penjelasan singkat run_sql.sh : ###

```
1. Truncate Semua Table (di file truncate_tables.sql
2. Jalankan semua script (kurang lebih seperti) : 
INSERT INTO db_target.table_target (fields) SELECT * FROM db_sumber.table_sumber
3. DONE.

```

### TODO ###

* Beberapa field referensi belum di join buat dapat value referensinya.
* Data deleted (Aktif = 'Tidak') masih dimasukkan
* Data upah ABK bisa direferensi dari db_pipp.mst_umr
* Filter GT dan Nama Kapal bisa di join referensinya berdasasrkan id_kapal (khusus buat data2 < 2014)
