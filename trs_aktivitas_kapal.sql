INSERT INTO
db_master.trs_aktivitas_kapal 
(
 `jenis_izin`		
,`aktivitas`		
,`filter_gt`		
,`id_pelabuhan`		
,`id_kapal`		
,`tgl_catat`		
,`tgl_aktivitas`		
,`nama_kapal`		
,`no_sipi`		
,`tanda_selar`		
,`gt_kapal`		
,`nama_kategori_kapal`		
,`loa`		
,`nama_pemilik`		
,`izin_wpp`		
,`izin_pelabuhan_pangkalan`		
,`izin_alat_tangkap`		
,`nama_nahkoda`		
,`jumlah_abk`		
,`biaya_tambat_labuh`		
,`upah_abk`		
,`upah_nakhoda`		
,`id_pelabuhan_tujuan`		
,`nama_pelabuhan_tujuan`		
,`id_pelabuhan_masuk`		
,`nama_pelabuhan_masuk`		
,`id_pelabuhan_asal`		
,`nama_pelabuhan_asal`		
,`jml_hari_trip`		
,`tujuan_berangkat`		
,`id_pelabuhan_docking`		
,`id_dpi`		
,`nama_dpi`		
,`id_wpp`		
,`id_alat_tangkap`		
,`nama_alat_tangkap`		
,`id_alat_bantu`		
,`alat_bantu_tangkap`		
,`tanggal_buat`		
,`id_aktivitas_referensi`		
,`aktif`		
,`id_pengguna_buat`		
,`id_pengguna_ubah`		
,`tanggal_ubah`		
,`nomor_spb`		
,`nomor_rekom`		
,`tanggal_rekom`		
,`jumlah_rekom`		
,`jumlah_realisasi`		
,`perkiraan_operasi`		
,`id_dermaga`)

SELECT NULL AS `jenis_izin`		
, tak.`aktivitas`		
, tak.`filter_gt`		
, tak.`id_pelabuhan`		
, tak.`id_kapal`		
, tak.`tgl_catat`		
, tak.`tgl_aktivitas`		
, tak.`nama_kapal`		
, NULL AS `no_sipi`		
, NULL AS `tanda_selar`		
, NULL AS `gt_kapal`		
, NULL AS `nama_kategori_kapal`		
, NULL AS `loa`		
, NULL AS `nama_pemilik`		
, NULL AS `izin_wpp`		
, NULL AS `izin_pelabuhan_pangkalan`		
, NULL AS `izin_alat_tangkap`		
, tak.`nama_nahkoda`		
, tak.`jumlah_abk`		
, NULL AS `biaya_tambat_labuh`		
, NULL AS `upah_abk`		
, NULL AS `upah_nakhoda`		
, tak.`id_pelabuhan_tujuan`		
, idt.nama_pelabuhan AS `nama_pelabuhan_tujuan`		
, tak.`id_pelabuhan_masuk`		
, idm.nama_pelabuhan AS `nama_pelabuhan_masuk`		
, tak.`id_pelabuhan_asal`		
, ida.nama_pelabuhan AS `nama_pelabuhan_asal`		
, tak.`jml_hari_trip`		
, tak.`tujuan_berangkat`		
, tak.`id_pelabuhan_docking`		
, tak.`id_dpi`		
, dpi.nama_dpi AS `nama_dpi`		
, tak.`id_wpp`		
, tak.`id_alat_tangkap`		
, mat.nama_alat_tangkap AS `nama_alat_tangkap`		
, tabk.`id_alat_bantu`		
, mab.nama_alat_bantu AS `alat_bantu_tangkap`		
, tak.`tanggal_buat`		
, tak.`id_aktivitas_referensi`		
, tak.`aktif`		
, tak.`id_pengguna_buat`		
, tak.`id_pengguna_ubah`		
, tak.`tanggal_ubah`		
, tak.`nomor_spb`		
, tak.`nomor_rekom`		
, tak.`tanggal_rekom`		
, tak.`jumlah_rekom`		
, tak.`jumlah_realisasi`		
, tak.`perkiraan_operasi`		
, tak.`id_dermaga`
FROM db_pipp.trs_aktivitas_kapal tak
LEFT JOIN db_master.mst_pelabuhan idt ON idt.id_pelabuhan = tak.id_pelabuhan_tujuan
LEFT JOIN db_master.mst_pelabuhan idm ON idm.id_pelabuhan = tak.id_pelabuhan_masuk
LEFT JOIN db_master.mst_pelabuhan ida ON ida.id_pelabuhan = tak.id_pelabuhan_asal
LEFT JOIN db_master.mst_dpi dpi ON dpi.id_dpi = tak.id_dpi
LEFT JOIN db_master.mst_alat_tangkap mat ON mat.id_alat_tangkap = tak.id_alat_tangkap
LEFT JOIN db_pipp.trs_alat_bantu_keluar tabk ON tabk.id_aktivitas_kapal = tak.id_aktivitas_kapal
LEFT JOIN db_pipp.mst_alat_bantu mab ON mab.id_alat_bantu = tabk.id_alat_bantu
GROUP BY tak.id_aktivitas_kapal

			
